# PIO Template

This project is a template that I use as a skeleton for my ESP8266/ESP32-based hardware
projects.


## Usage

To use this template:

1. Clone the repository somewhere.
1. Change the project name in `.config`.
1. Change the README.
1. Write some code.


## Configuration

For its configuration, this project uses a file called `.config`.  It's a simple
INI-style file that looks like this:

```
VAR=value
ANOTHER_VAR=othervalue
```

The build script uses values in this file as `#define`s in the program, so you can
easily specify project-specific values.


## The build script

The build script is a simple Python script that does a few things:

1. Reads `.config` and adds the variables there as `#define`s.
1. Reads `.secrets` and adds the variables there as `#define`s.
1. Reads and increments `.version` to create the `VERSION` `#define`.
1. Builds and copies or flashes the firmware.

The versioning system is geared towards usage with
[espota-server](https://gitlab.com/stavros/espota-server), to allow for pull-based OTA
updates. This way, your devices can update themselves by talking to the ESPOTA server.


* To just build the code (e.g. to see if there are errors), just run `./build`.
* To build and flash the code to USB, run `./build usb`.
* To build and copy the resulting file to a dir, run `./build /some/dir/`
* To build and flash the code to a USB specifying the port, run `./build /dev/ttyPORT`.

I generally use ESPOTA and use `./build /path/to/espota/dir/` to copy the firmware there
with the appropriate filename (`PROJECTNAME-version.bin`), and then reboot the device.
This is also the fastest way to flash, as downloads over WiFi are much faster than over
serial.


## StavrosUtils

StavrosUtils is a class with various functions I like to use. There are some debugging
functions and an HTTP update function over verified TLS. I recommend you run a server
like Caddy with a self-signed certificate and run ESPOTA server behind that, to get
secure upgrades over the internet with one line of code.


## Epilogue

That's all! Please let me know in the issues if you have any feedback or improvements.
